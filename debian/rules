#!/usr/bin/make -f

# See 'man dpkg-architecture'
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

CMAKE_FLAGS=

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
  CFLAGS += -O0
  CMAKE_FLAGS += -Denable_compile_optimizations=off
else
  CFLAGS += $(shell dpkg-buildflags --get CFLAGS)
  # build with -O3 everywhere since it is upstream policy to do so.
  CFLAGS += -O3
endif

ifneq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
  CMAKE_FLAGS += -Denable_documentation=off
endif

export DEB_BUILD_MAINT_OPTIONS += hardening=+all

export CXXFLAGS=$(shell dpkg-buildflags --get CXXFLAGS)
export CPPFLAGS=$(shell dpkg-buildflags --get CPPFLAGS) -DUSE_GLES2
export LDFLAGS=$(shell dpkg-buildflags --get LDFLAGS)

%:
	dh $@

override_dh_auto_build: frogatto.6
	dh_auto_build USE_CCACHE=no

frogatto.6: debian/frogatto.pod
	pod2man --section=6 $< > $@

override_dh_clean:
	dh_clean
	find -name \*.o -delete
	find -name \*.mo -delete
	find -name \*.d -delete
	rm -f game frogatto.6

override_dh_gencontrol:
	dh_gencontrol -- -VUVersion=$(DEB_VERSION_UPSTREAM)

tarball:
	cd .. && tar \
		--exclude=debian \
		--exclude=.git \
		-czvf frogatto_$(PACKAGE_VERSION).orig.tar.gz \
		frogatto-$(PACKAGE_VERSION)
